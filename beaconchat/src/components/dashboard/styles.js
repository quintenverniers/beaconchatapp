const styles = theme => ({
    signOutBtn: {
      position: 'absolute',
      bottom: '0px',
      left: '0px',
      width: '300px',
      borderRadius: '0px',
      height: '35px',
      color: 'white'
    },
    rightIcon: {
      marginLeft: theme.spacing(1),
    }
  });
  
  export default styles;