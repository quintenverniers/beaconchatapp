import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import { Route, BrowserRouter as Router } from 'react-router-dom';
import LoginComponent from './components/login/login';
import SignupComponent from './components/signup/signup';
import DashComponent from './components/dashboard/dashboard';

const firebase = require('firebase');
require("firebase/firestore");

firebase.initializeApp({
    apiKey: "AIzaSyALdUA46FKSFFSBz6i1QWYHzlkYiLYFNZQ",
    authDomain: "beaconchat-b3e65.firebaseapp.com",
    databaseURL: "https://beaconchat-b3e65.firebaseio.com",
    projectId: "beaconchat-b3e65",
    storageBucket: "beaconchat-b3e65.appspot.com",
    messagingSenderId: "460073887498",
    appId: "1:460073887498:web:625cb04537a612b3"
});

const routing = (
    <Router>
        <div id="routing-container">
            <Route path="/login" component={LoginComponent}></Route>
            <Route path="/signup" component={SignupComponent}></Route>
            <Route path="/dashboard" component={DashComponent}></Route>
        </div>
    </Router>
)

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
